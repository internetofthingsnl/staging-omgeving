@extends('layouts.admin')

@section('content')
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">CPU Traffic</span>
                        <span class="info-box-number">90<small>%</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">New Members</span>
                        <span class="info-box-number">2,456</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Nieuwe winkel aanmaken</h3>
                </div>
                <!-- /.box-header -->

                <!-- form start -->
                <form {{url('addstore')}} method="POST">

                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputNaam">Naam van shophouder</label>
                            <input type="text" class="form-control" name="shophouder" id="Naam_van_shophouder" placeholder="Vul naam van shophouder in.">
                        </div>
                        <div class="form-group">
                            <label for="bedrijf">Naam van het bedrijf</label>
                            <input type="text" class="form-control" name="bedrijf"  id="bedrijf" placeholder="Vul naam van het bedrijf in.">
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<label for="exampleInputPassword1">Password</label>--}}
                            {{--<input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password">--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="exampleInputFile">File input</label>--}}
                            {{--<input type="file" id="exampleInputFile">--}}

                            {{--<p class="help-block">Example block-level help text here.</p>--}}
                        {{--</div>--}}
                        {{--<div class="checkbox">--}}
                            {{--<label>--}}
                                {{--<input type="checkbox"> checkbox example--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Aanmaken</button>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
        <!-- /.row -->

    </section>

@endsection