<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/add_store', function () {
    return view('add_store');
});

Route::get('/check_store', function () {
    return view('check_store');
});

Route::post('/add_store', 'addstore@createdir');

Route::post('/add_store', 'addstore@create_subdomain');


Route::get('/home', 'HomeController@index')->name('home');
