<?php $__env->startSection('content'); ?>
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">CPU Traffic</span>
                        <span class="info-box-number">90<small>%</small></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">New Members</span>
                        <span class="info-box-number">2,456</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Nieuwe winkel aanmaken</h3>
                </div>
                <!-- /.box-header -->

                <!-- form start -->
                <form <?php echo e(url('addstore')); ?> method="POST">

                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputNaam">Naam van shophouder</label>
                            <input type="text" class="form-control" name="shophouder" id="Naam_van_shophouder" placeholder="Vul naam van shophouder in.">
                        </div>
                        <div class="form-group">
                            <label for="bedrijf">Naam van het bedrijf</label>
                            <input type="text" class="form-control" name="bedrijf"  id="bedrijf" placeholder="Vul naam van het bedrijf in.">
                        </div>
                        
                            
                            
                        
                        
                            
                            

                            
                        
                        
                            
                                
                            
                        
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Aanmaken</button>
                    </div>
                    <?php echo csrf_field(); ?>
                </form>
            </div>
        </div>
        <!-- /.row -->

    </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>